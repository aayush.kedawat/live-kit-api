'use strict'
exports.__esModule = true
var livekit_server_sdk_1 = require('livekit-server-sdk')

var livekitHost = process.env.LIVEKITHOST

var svc = new livekit_server_sdk_1.RoomServiceClient(
  livekitHost,
  process.env.LIVEKITAPI,
  process.env.LIVEKITSECRET
)
// list rooms

// create a new room

exports.createRoom = async (req, res) => {
    console.log(req.body);
  //  createNewRoom(roomName, maxParticipants)
  var opts = {
    name: req.body.roomName,
    // timeout in seconds
    emptyTimeout: 10 * 60,
    maxParticipants: req.body.maxParticipants
  }
  svc.createRoom(opts).then(function (room) {
    // createAccessToken(room.name)

    var token = createAccessToken(req.body.participantName, req.body.isPublisher, room.name);
    return res.status(200).json(
        
     {       "status":"success",
            "status_code": 200,
            "access_token": token}
     )
  })
}

exports.listOfRooms = async (req, res) => {
  svc.listRooms().then(function (rooms) {
    res.status(200).json(
        {
            "status":"success",
            "status_code": 200,
            listOfRooms: rooms
        }
    )
  })
}

exports.getUserAccessToken = async (req, res) =>{

    var token = createAccessToken(req.body.participantName, req.body.isPublisher, req.body.roomName);
    return res.status(200).json(
        {
            "status":"success",
            "status_code": 200,
            "access_token": token
        }
    )
}

function createAccessToken (participantName, isPublisher, roomName) {
    console.log(participantName)
  var at = new livekit_server_sdk_1.AccessToken(
    process.env.LIVEKITAPI,
    process.env.LIVEKITSECRET,
    {
      identity: participantName
    }
  )
  at.addGrant({ roomJoin: true, room: roomName, canPublish: isPublisher })
  var token = at.toJwt()
  return token
}

// });
// if this room doesn't exist, it'll be automatically created when the first
// client joins
// delete a room
// svc.deleteRoom('myroom').then(() => {
//   console.log('room deleted');
// });
