/* eslint-disable max-len */
"use strict";
// exports.__esModule = true
const livekitServerSDK = require("livekit-server-sdk");
const apiKey = "APIz2YxGbD6J9y4";
const apiSecret = "Akm5wxORtUrEF7AkaHp4RBBT0AXGihhNt5EMZmOQ5iE";

const livekitHost = "https://lk.livekitatrina.com"

const svc = new livekitServerSDK.RoomServiceClient(
  livekitHost,
  apiKey,
  apiSecret
);
// list rooms

// create a new room

exports.createRoom = ((req, res) => {
  console.log(req.body);
  //  createNewRoom(roomName, maxParticipants)
  const opts = {
    name: req.body.roomName,
    // timeout in seconds
    emptyTimeout: 10 * 60,
    maxParticipants: req.body.maxParticipants,
  };
  svc.createRoom(opts).then(function(room) {
    // createAccessToken(room.name)

    const token = createAccessToken(req.body.participantName, req.body.isPublisher, room.name);
    return res.status(200).json(
        {"status": "success",
          "status_code": 200,
          "access_token": token}
    );
  });
});

exports.listOfRooms = ((req, res) => {
  svc.listRooms().then(function(rooms) {
    res.status(200).json(
        {
          "status": "success",
          "status_code": 200,
          "listOfRooms": rooms,
        }
    );
  });
});

exports.getUserAccessToken = ((req, res) => {
  const token = createAccessToken(req.body.participantName, req.body.isPublisher, req.body.roomName);
  return res.status(200).json(
      {
        "status": "success",
        "status_code": 200,
        "access_token": token,
      }
  );
});

// eslint-disable-next-line require-jsdoc
function createAccessToken(participantName, isPublisher, roomName) {
  console.log(participantName);
  const at = new livekitServerSDK.AccessToken(
      apiKey,
      apiSecret,
      {
        identity: participantName,
      }
  );
  at.addGrant({roomJoin: true, room: roomName, canPublish: isPublisher, canPublishData: isPublisher});
  const token = at.toJwt();
  return token;
}

// });
// if this room doesn't exist, it'll be automatically created when the first
// client joins
// delete a room
// svc.deleteRoom('myroom').then(() => {
//   console.log('room deleted');
// });
