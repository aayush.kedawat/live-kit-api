const functions = require("firebase-functions");
const express = require("express");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const app = express();

// eslint-disable-next-line max-len
const {createRoom, getUserAccessToken, listOfRooms} = require("./handlers/room_controller");

// app.get("/listOfRooms", listOfRooms);
// app.post("/createRoom", jsonParser, createRoom );
// app.post("/createToken", jsonParser, getUserAccessToken);


// exports.app = functions.https.onRequest(listOfRooms);

exports.createRoom = functions.https.onRequest(createRoom);
exports.listOfRooms = functions.https.onRequest(listOfRooms);
exports.createToken = functions.https.onRequest(getUserAccessToken);
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//   functions.logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
