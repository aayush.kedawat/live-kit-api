var express = require('express');
require('dotenv').config()

var bodyParser = require('body-parser')
var jsonParser = bodyParser.json()

var app = express();
const PORT = process.env.PORT || 5050
app.get('/', (req, res) => {
res.send('This is my demo project')
})
const { createRoom, getUserAccessToken, listOfRooms } = require('./handlers/room_controller')
app.get('/listOfRooms', listOfRooms);
app.post('/createRoom', jsonParser, createRoom )
app.post('/createToken',jsonParser, getUserAccessToken);
app.listen(PORT, function () {
console.log(`Demo project at: ${PORT}!`); });